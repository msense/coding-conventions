# PHP Convention Guide
This is the Taplane PHP convention guide used amongst all PHP projects, specifically server and back end.

# Formatting & Syntax

1. Our main source of formatting syntax is via PSR-12 https://www.php-fig.org/psr/psr-12/
2. Classes typically follow CamelCase naming conventions, with wording which describes what they do/manipulate
3. PHP's built in serialization mechanism (the `serialize()` and `unserialize()` functions) should not be used for data stored (or read from) outside of the current process. Use JSON based serialization instead
4. It is essential that your code be well documented so that other developers and bug fixers can easily navigate the logic of your code. New classes, methods, and member variables should include comments providing brief descriptions of their functionality (unless it is obvious), even if private. In addition, all new methods should document their parameters and return values.
5. We will also be using the PHP file (attached) for maintaining project wide styling guides and .gitignores 
