# A curated list of coding conventions to follow
---

## ANDROID 
- [Android Styling & Conventions](./ANDROID_CONVENTIONS.md)

## PHP 
- [PHP Styling & Conventions](./PHP_CONVENTIONS.md)

## JAVASCRIPT 
- [JavaScript Styling & Conventions](./JAVASCRIPT_CONVENTIONS.md)

## CSHARP/UNITY 
- [CSharp Styling & Conventions](./CSHARP_GUIDELINES.md)