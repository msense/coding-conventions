# Csharp Coding Guidelines

Original Guideline: [https://wiki.unity3d.com/index.php/Csharp_Coding_Guidelines](https://wiki.unity3d.com/index.php/Csharp_Coding_Guidelines)
This guide shows how to format code to make readability easier, and code cleaner.

## Style Guidelines

Below are good practices for code styling

### 1. Bracing

 - Open braces should always be on same line as the statement that begins the block with a space in between. Contents of the brace should be indented by 1 tab or 4 spaces. For example

Code Example:

```
    if (someExpression) {
       DoSomething();
    } else {
       DoSomethingElse();
    }
```
 - cases in Switch blocks should be indented from the switch statement like this

Code Example:

```
    switch (someExpression) {
        case 0:
            DoSomething();
            break;
     
       case 1:
          DoSomethingElse();
          break;
     
       case 2: 
          {
             int n = 1;
             DoAnotherThing(n);
          }
          break;
    }
```
 - Braces should never be considered optional. Even for single statement blocks, you should always use braces. This increases code readability and maintainability. Which includes all controls statements (if, while, for, etc.)
 - Single line statements can have braces that begin and end on the same line. (If space allows it)

Code Example:
```
     for (int i = 0; i < 100; i++) { DoSomething(i); }
```
  ---

### 2. Commenting
Overview: Comments should be used to describe intention, algorithmic overview, and/or logical flow. It would be ideal, if from reading the comments alone, someone other than the author could understand a function�s intended behavior and general operation. While there are no minimum comment requirements and certainly some very small routines need no commenting at all, it is hoped that most routines will have comments reflecting the programmer�s intent and approach.

#### Comment Style

 - The // (two slashes) style of comment tags should be used in most situations. Where ever possible, place comments above the code instead of beside it. Here are some examples

Example:
```
    // This is required for Controller access for hit detection
    FPSController controller = hit.GetComponent<FPSController>();
```
 - Comments can be placed at the end of a line when space allows

Example:
```
    public class SomethingUseful {
        private int itemHash; // instance member
        private static bool hasDoneSomething; // static member
    }
```
#### Function Comments

 - When function needs a comment to clarify use ideally use:
```
    ///<summary> 
    /// Calculates the distance to finish line and returns percentage of level finished. 
    ///</summary>
    public void CalculateTravelPercentage() {
    	return travelled / TotalDistance(); 
    }
```
 ----
### 3. Spacing

 Overview: Spaces improve readability by decreasing code density. Here are some guidelines for the use of space characters within code:

 - Do use a single space after a comma between function arguments.     
**Right:** Console.In.Read(myChar, 0, 1);  
**Wrong:** Console.In.Read(myChar,0,1);

 - Do not use a space after the parenthesis and function arguments.  
   **Right:** CreateFoo(myChar, 0, 1);  
   **Wrong:** CreateFoo( myChar, 0, 1 );

 - Do not use spaces between a function name and parenthesis.  
   **Right:** CreateFoo();  
   **Wrong:** CreateFoo ();

 - Do not use spaces inside brackets.  
   **Right:** x = dataArray[index];  
   **Wrong:** x = dataArray[ index ];

 - Do use a single space before flow control statements.  
   **Right:** while (x == y)  
   **Wrong:** while(x==y)

 - Do use a single space before and after comparison operators  
   **Right:** if (x == y)  
   **Wrong:** if (x==y)

 - Do use a single space before and after operators  
   **Right:** z = x + y;  
   **Wrong:** z = x+y;

 - Leave a blank line between functions

---
### 4. Naming

 -   **Do**  use camelCasing for variables, parameters, local variables
 -   **Do**  use PascalCasing for function, class names
 -   **Do**  use CAPITALIZED_WITH_UNDERSCORES for const variables
---
### 5. File Organization
 - Overview: Variables are defined first then functions
 - Static Variables should be at the top
 - MonoBehaviour lifecycle functions should be above all other functions. MonoBehaviour functions found here:  [https://docs.unity3d.com/ScriptReference/MonoBehaviour.html](https://docs.unity3d.com/ScriptReference/MonoBehaviour.html)
 - Order of MonoBehaviour of commonly used functions: Awake, Start, FixedUpdate, OnTriggerXXX, OnCollisionXXX, Update, LateUpdate, OnApplictionPause, OnApplicationQuit
